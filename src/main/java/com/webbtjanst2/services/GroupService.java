package com.webbtjanst2.services;

import com.webbtjanst2.dto.CreateGroup;
import com.webbtjanst2.dto.GroupDTO;
import com.webbtjanst2.entity.Group;
import com.webbtjanst2.repository.GroupRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class GroupService {

    GroupRepository groupRepository;

    public Stream<Group> getGroups() {
        return groupRepository.findAll().stream();
    }

    public Optional<Group> create(String personId, CreateGroup createGroup) {
        List<String> members=new ArrayList<>();
        members.add(UUID.fromString(personId).toString());
        Group group=new Group(UUID.randomUUID().toString(),createGroup.getName(),members);
        groupRepository.save(group);
        return Optional.of(group);
    }

    public void delete(String groupId) {
       groupRepository.delete(groupId);
    }

    public  Stream<Group> getGroup(String memberId) {
        return groupRepository.findAll().stream().filter(g -> g.getMembers().contains(memberId));
    }

    public String getGroupName(String groupId) {
        return groupRepository.findGroupName(groupId);
    }

    public String updateGroupName(String groupId, String name) {
        return groupRepository.updateGroupName(groupId,name);
    }

    public void deleteMemberFromGroup(String memberId) {
            groupRepository.deleteByMemberId(memberId);
    }

    public void deleteMemberAndGroup(String groupId, String memberId) {
        groupRepository.deleteMemberAndGroup(groupId,memberId);
    }
}
