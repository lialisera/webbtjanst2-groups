package com.webbtjanst2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class    WebbTjanst2Application {

    public static void main(String[] args) {
        SpringApplication.run(WebbTjanst2Application.class, args);
    }

}
