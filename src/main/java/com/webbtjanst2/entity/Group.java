package com.webbtjanst2.entity;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class Group {

    private String id;

    private String name;
    private List<String> members;

    public Group (String id,String name, List<String> members){
        this.id = id;
        this.name = name;
        this.members = members;
    }
}

