package com.webbtjanst2.controllers;

import com.webbtjanst2.dto.CreateGroup;
import com.webbtjanst2.dto.GroupDTO;
import com.webbtjanst2.entity.Group;
import com.webbtjanst2.services.GroupService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Webtjänst 2 - Grupper
 * Någon form av säkerhet
 * Token
 * Pre shared secret
 * JWT (För VG)
 * Grupp (id, gruppnamn, medlemmar [lista med id])
 * hämta gruppnamn (via grupp id) (find group name by id)->done
 * skapa grupp (create group)->done
 * byt namn på grupp (update name)-> done
 * radera grupp (delete group)-> done
 */
@RestController
@RequestMapping("/api/group")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")

public class GroupController {
    GroupService groupService;

    private static final String CODESECURITY = "123";


    @GetMapping()
    public List<GroupDTO> getGroups(@RequestHeader("codekey") String codekey) {
        if (!CODESECURITY.equals(codekey)) {
            return null;
        }
        return groupService.getGroups().map(this::toGroupDTO).collect(Collectors.toList());
    }

    @PostMapping("/{personId}")
    public GroupDTO createGroup(@RequestHeader(value = "personId", required = true) String personId, @RequestHeader("codekey") String codekey, @RequestBody CreateGroup createGroup) throws Exception {
        if (!CODESECURITY.equals(codekey)) {
            return null;
        }
        return groupService.create(personId, createGroup).map(this::toGroupDTO).get();
    }

    @GetMapping("/{memberId}")
    public List<GroupDTO> getGroups(@RequestHeader(value = "memberId", required = true) String memberId, @RequestHeader("codekey") String codekey) {
        if (!CODESECURITY.equals(codekey)) {
            return null;
        }
        return groupService.getGroup(memberId).map(this::toGroupDTO).collect(Collectors.toList());
    }

    @GetMapping("/{groupId}")
    public String getGroupName(@RequestHeader(value = "groupId", required = true) String groupId, @RequestHeader("codekey") String codekey) {
        if (!CODESECURITY.equals(codekey)) {
            return null;
        }
        return groupService.getGroupName(groupId);
    }

    @PutMapping()
    public String updateGroupName(@RequestHeader(value = "groupId", required = true) String groupId, @RequestHeader("codekey") String codekey, @RequestHeader("name") String name) {
        if (!CODESECURITY.equals(codekey)) {
            return null;
        }
        return groupService.updateGroupName(groupId, name);
    }

   // @DeleteMapping("/delete/{id}")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void deleteGroup(@RequestHeader(value = "id", required = true) String id, @RequestHeader("codekey") String codekey) {
        if (CODESECURITY.equals(codekey)) {
            groupService.delete(id);
        }
    }

    // @DeleteMapping("/delete/{memberId}")
    @RequestMapping(value = "/delete/{memberId}", method = RequestMethod.DELETE)
    public void deleteMember(@RequestHeader(value = "memberId", required = true) String memberId, @RequestHeader("codekey") String codekey) {
        if (CODESECURITY.equals(codekey)) {
            groupService.deleteMemberFromGroup(memberId);
        }
    }

    @RequestMapping(value = "/delete/{groupId}/{memberId}", method = RequestMethod.DELETE)
    public void deleteMemberAndGroup(@PathVariable(value = "groupId") String groupId,@PathVariable(value = "memberId") String memberId, @RequestHeader("codekey") String codekey) {
        if (CODESECURITY.equals(codekey)) {

            groupService.deleteMemberAndGroup(groupId,memberId);
        }
    }

    private GroupDTO toGroupDTO(Group group) {
        return new GroupDTO(group.getId(),
                group.getName(),
                (group.getMembers() != null) ? group.getMembers().stream().collect(Collectors.toList()) : new ArrayList<>());
    }

}

