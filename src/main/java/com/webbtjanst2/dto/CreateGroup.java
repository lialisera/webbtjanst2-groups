package com.webbtjanst2.dto;

import lombok.Data;

@Data
public class CreateGroup {
    String name;
}

