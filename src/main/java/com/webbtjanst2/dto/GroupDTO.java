package com.webbtjanst2.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class GroupDTO {

    String id;
    String name;
    List<String> members;

    @JsonCreator
    public GroupDTO(
            @JsonProperty("id")String id,
            @JsonProperty("name")String name,
            @JsonProperty("members")List<String> members){
        this.id=id;
        this.name=name;
        this.members=members;
   }
}
