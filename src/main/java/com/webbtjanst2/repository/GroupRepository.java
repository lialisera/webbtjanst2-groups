package com.webbtjanst2.repository;

import com.webbtjanst2.entity.Group;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Stream;

@Repository
public class GroupRepository   {
    Map<String, Group> groups=new HashMap<>();
    public Collection<Group> findAll(){
        return groups.values();
    }

    public void save(Group group) {
        groups.put(group.getId(),group);
    }

    public void delete(String groupId){
        groups.remove(groupId);
   }

    public String findGroupName(String groupId) {
        return groups.get(groupId).getName();
    }

    public String updateGroupName(String groupId, String name) {
        String oldGroupName=findGroupName(groupId);
        Group oldGroup = groups.values().stream().filter(g -> g.getName().equals(oldGroupName)).findFirst().get();
        oldGroup.setName(name);
        Group newGroup = groups.put(groupId, oldGroup);
        return newGroup.getName();
    }

    public void deleteByMemberId(String memberId) {
            Group group=groups.values().stream().filter(g->g.getMembers().contains(memberId)).findFirst().get();
            group.setId(group.getId());
            group.setName(group.getName());
            group.setMembers(null);
            groups.put(group.getId(),group);
    }

    public void deleteMemberAndGroup(String groupId, String memberId) {
      Group group = groups.values().stream().filter(g->g.getId().equals(groupId))
              .filter(g->g.getMembers().contains(memberId)).findFirst().get();
      groups.remove(group.getId());

    }
}

